package io.swsb;

import java.lang.annotation.*;

/**
 * Created by swsb.
 */
@Documented
@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface RestDoc
{
    /**
     * Http Method
     */
    HttpMethod method();

    /**
     * Http Responses
     */
    HttpResponse[] httpResponses();

    /**
     * Route Name
     */
    String name();

    /**
     * Route Description
     */
    String description();

    /**
     * Path
     */
    String path();

    /**
     * Response Content-Type
     */
    String contentType() default "application/json";

    /**
     * Request Headers
     */
    String[] headers() default {};

    /**
     * Response Headers
     */
    String[] responseHeaders() default {};

    /**
     * Authenticated
     */
    boolean authenticated() default true;

    /**
     * Roles allowed
     */
    String[] roles() default {"user"};

    Class requestBody() default Class.class;

    Class responseBody() default Class.class;
}
