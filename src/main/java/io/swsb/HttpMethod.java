package io.swsb;

/**
 * Created by swsb.
 */
public enum HttpMethod
{
    GET,
    DELETE,
    HEAD,
    OPTIONS,
    PUT,
    POST,
    PATCH;
}
