package io.swsb;

import java.lang.annotation.*;

/**
 * Created by swsb.
 */
@Documented
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface HttpResponse
{
    /**
     * HTTP Status code
     */
    int statusCode();

    /**
     * Reason the status code is being returned
     */
    String reason();
}
