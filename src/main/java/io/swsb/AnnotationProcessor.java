package io.swsb;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.bval.constraints.Email;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import javax.validation.constraints.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by swsb.
 */
@SupportedAnnotationTypes("io.swsb.RestDoc")
public class AnnotationProcessor extends AbstractProcessor
{
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static Map<String, Map<String, Object>> doc = new HashMap<>();


    public AnnotationProcessor()
    {
    }


    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
    {
        annotations.forEach(t -> {
            roundEnv.getElementsAnnotatedWith(t).stream().forEach(e -> {
                RestDoc restDoc = e.getAnnotation(RestDoc.class);

                String key = e.getEnclosingElement().getSimpleName().toString() + "#" + e.getSimpleName().toString();

                if (!doc.containsKey(key))
                {
                    Map<String, Object> map = processRestDoc(restDoc, e);
                    doc.put(key, map);
                }
            });
        });

        if (roundEnv.processingOver())
        {

            try
            {
                FileObject sourceFile = processingEnv.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, "", "restDoc.json");
                PrintWriter out = new PrintWriter(sourceFile.openWriter());
                out.write(objectMapper.writeValueAsString(doc.entrySet()));
                out.flush();
                out.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }


        return false;
    }


    public Map<String, Object> processRestDoc(RestDoc restDoc, Element e)
    {

        Map<String, Object> restDocMap = new HashMap<>();
        restDocMap.put("name", restDoc.name());
        restDocMap.put("description", restDoc.description());
        restDocMap.put("method", restDoc.method());
        restDocMap.put("routeDoc", processingEnv.getElementUtils().getDocComment(e));

        List<Map<String, Object>> httpResponses = new ArrayList<>();

        for (HttpResponse response : restDoc.httpResponses())
        {
            Map<String, Object> httpResponse = new HashMap<>();
            httpResponse.put("statusCode", response.statusCode());
            httpResponse.put("reason", response.reason());
            httpResponses.add(httpResponse);
        }

        restDocMap.put("httpResponses", httpResponses);
        restDocMap.put("path", restDoc.path());
        restDocMap.put("contentType", restDoc.contentType());
        restDocMap.put("headers", restDoc.headers());
        restDocMap.put("responseHeaders", restDoc.responseHeaders());
        restDocMap.put("authenticated", restDoc.authenticated());
        restDocMap.put("roles", restDoc.roles());

        try
        {
            restDoc.requestBody();
        }
        catch (MirroredTypeException ex)
        {
            TypeMirror typeMirror = ex.getTypeMirror();
            TypeElement typeElement = (TypeElement) processingEnv.getTypeUtils().asElement(typeMirror);
            List<Map<String, Object>> fieldList = new ArrayList<>();
            if (!typeElement.getSimpleName().toString().equals("Class"))
            {
                List<? extends Element> enclosedElements = typeElement.getEnclosedElements();
                for (Element element : enclosedElements)
                {
                    if (element.getKind().isField())
                    {
                        Map<String, Object> body = new HashMap<>();
                        body.put("type", element.asType().toString());
                        body.put("name", element.getSimpleName().toString());
                        body.put("description", processingEnv.getElementUtils().getDocComment(element));
                        body.put("constraints", getConstraints(element));
                        fieldList.add(body);
                    }
                }
            }
            restDocMap.put("requestBody", fieldList);
        }

        try
        {
            restDoc.responseBody();
        }
        catch (MirroredTypeException ex)
        {
            TypeMirror typeMirror = ex.getTypeMirror();
            TypeElement typeElement = (TypeElement) processingEnv.getTypeUtils().asElement(typeMirror);
            List<Map<String, Object>> fieldList = new ArrayList<>();
            if (typeElement != null && typeElement.getSimpleName() != null && typeElement.getSimpleName().toString() != null && !typeElement.getSimpleName().toString().equals("Class"))
            {
                List<? extends Element> enclosedElements = typeElement.getEnclosedElements();
                for (Element element : enclosedElements)
                {
                    if (element.getKind().isField())
                    {
                        Map<String, Object> body = new HashMap<>();
                        body.put("type", element.asType().toString());
                        body.put("name", element.getSimpleName().toString());
                        body.put("description", processingEnv.getElementUtils().getDocComment(element));
                        body.put("constraints", getConstraints(element));
                        fieldList.add(body);
                    }
                }
            }
            restDocMap.put("responseBody", fieldList);
        }

        return restDocMap;
    }

    private Map<String, Object> getConstraints(Element element)
    {

        Map<String, Object> constraints = new HashMap<>();
        constraints.put("allowNull", element.getAnnotation(NotNull.class) != null);
        if (element.getAnnotation(Size.class) != null)
        {
            constraints.put("minLength", element.getAnnotation(Size.class).min());
            constraints.put("maxLength", element.getAnnotation(Size.class).max());
        }
        if (element.getAnnotation(Email.class) != null)
        {
            constraints.put("isEmail", true);
        }
        if (element.getAnnotation(DecimalMax.class) != null)
        {
            constraints.put("decimalMax", element.getAnnotation(DecimalMax.class).value());
        }
        if (element.getAnnotation(DecimalMin.class) != null)
        {
            constraints.put("decimalMin", element.getAnnotation(DecimalMin.class).value());
        }
        if (element.getAnnotation(Future.class) != null)
        {
            constraints.put("inFuture", true);
        }
        if (element.getAnnotation(Past.class) != null)
        {
            constraints.put("inPast", true);
        }
        if (element.getAnnotation(Max.class) != null)
        {
            constraints.put("max", element.getAnnotation(Max.class).value());
        }
        if (element.getAnnotation(Min.class) != null)
        {
            constraints.put("min", element.getAnnotation(Min.class).value());
        }
        if (element.getAnnotation(Pattern.class) != null)
        {
            constraints.put("pattern", element.getAnnotation(Pattern.class).regexp());
        }
        return constraints;
    }
}